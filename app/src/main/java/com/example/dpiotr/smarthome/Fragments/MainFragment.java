package com.example.dpiotr.smarthome.Fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.dpiotr.smarthome.Activities.SmartHomeActivity;
import com.example.dpiotr.smarthome.R;
import com.example.dpiotr.smarthome.RecyclerItemClickListener;
import com.example.dpiotr.smarthome.Resource;
import com.example.dpiotr.smarthome.ResourceAdapter;

import static com.example.dpiotr.smarthome.Activities.SmartHomeActivity.PREF_IP;
import static com.example.dpiotr.smarthome.Activities.SmartHomeActivity.PREF_PORT;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    private static List<Resource> resourceList = new ArrayList<>();
    private RecyclerView recyclerView;
    private static ResourceAdapter mAdapter;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_main, container, false);

        recyclerView = (RecyclerView) layout.findViewById(R.id.recycler_view);

        mAdapter = new ResourceAdapter(resourceList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);

        if(resourceList.isEmpty()) {
            resourceList.add(new Resource("Pin 11", "state"));
            resourceList.add(new Resource("Pin 12", "state"));
        }
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // get the pin number
                String parameterValue = "";
                // get the ip address
                String ipAddress = SettingsFragment.getEditTextIPAddress().getText().toString().trim();
                // get the port number
                String portNumber = SettingsFragment.getEditTextPortNumber().getText().toString().trim();

                // save the IP address and port for the next time the app is used
                SmartHomeActivity.editor.putString(PREF_IP,ipAddress); // set the ip address value to save
                SmartHomeActivity.editor.putString(PREF_PORT,portNumber); // set the port number to save
                SmartHomeActivity.editor.commit(); // save the IP and PORT;

                if(position==0) parameterValue="11";
                else parameterValue="12";

                if(ipAddress.length()>0 && portNumber.length()>0){
                    new MainFragment.HttpRequestAsyncTask(
                            view.getContext(),parameterValue,ipAddress,portNumber,"pin"
                    ).execute();
                }
            }
        }));

        return layout;
    }

    /**
     * An AsyncTask is needed to execute HTTP requests in the background so that they do not
     * block the user interface.
     */
    private class HttpRequestAsyncTask extends AsyncTask<Void, Void, Void> {

        // declare variables needed
        private String requestReply,ipAddress, portNumber;
        private Context context;
        private AlertDialog alertDialog;
        private String parameter;
        private String parameterValue;

        /**
         * Description: The asyncTask class constructor. Assigns the values used in its other methods.
         * @param context the application context, needed to create the dialog
         * @param parameterValue the pin number to toggle
         * @param ipAddress the ip address to send the request to
         * @param portNumber the port number of the ip address
         */
        public HttpRequestAsyncTask(Context context, String parameterValue, String ipAddress, String portNumber, String parameter)
        {
            this.context = context;

            alertDialog = new AlertDialog.Builder(this.context)
                    .setTitle("HTTP Response From IP Address:")
                    .setCancelable(true)
                    .create();

            this.ipAddress = ipAddress;
            this.parameterValue = parameterValue;
            this.portNumber = portNumber;
            this.parameter = parameter;
        }

        /**
         * Name: doInBackground
         * Description: Sends the request to the ip address
         * @param voids
         * @return
         */
        @Override
        protected Void doInBackground(Void... voids) {
            alertDialog.setMessage("Data sent, waiting for reply from server...");
            if(!alertDialog.isShowing())
            {
                alertDialog.show();
            }
            requestReply = SmartHomeActivity.sendRequest(parameterValue,ipAddress,portNumber, parameter);
            return null;
        }

        /**
         * Name: onPostExecute
         * Description: This function is executed after the HTTP request returns from the ip address.
         * The function sets the dialog's message with the reply text from the server and display the dialog
         * if it's not displayed already (in case it was closed by accident);
         * @param aVoid void parameter
         */
        @Override
        protected void onPostExecute(Void aVoid) {
            alertDialog.setMessage(requestReply);
            Resource res = new Resource("","");
            int i = Integer.parseInt(requestReply.substring(4,6));
            if(i==11) {res = resourceList.get(0);}
            else {res = resourceList.get(1);}
            res.setDescription(requestReply.substring(10));
            if(i==11){
                resourceList.add(0,res);
                resourceList.remove(1);
                mAdapter.notifyDataSetChanged();
            }else{
                resourceList.add(1,res);
                resourceList.remove(2);
                mAdapter.notifyDataSetChanged();
            }


            if(!alertDialog.isShowing())
            {
                alertDialog.show(); // show dialog
            }
        }

        /**
         * Name: onPreExecute
         * Description: This function is executed before the HTTP request is sent to ip address.
         * The function will set the dialog's message and display the dialog.
         */
        @Override
        protected void onPreExecute() {
            alertDialog.setMessage("Sending data to server, please wait...");
            if(!alertDialog.isShowing())
            {
                alertDialog.show();
            }
        }

    }
}
