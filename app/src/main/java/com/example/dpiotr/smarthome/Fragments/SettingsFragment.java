package com.example.dpiotr.smarthome.Fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.dpiotr.smarthome.Activities.SmartHomeActivity;
import com.example.dpiotr.smarthome.R;

import static com.example.dpiotr.smarthome.Activities.SmartHomeActivity.PREF_IP;
import static com.example.dpiotr.smarthome.Activities.SmartHomeActivity.PREF_PORT;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    private static EditText editTextIPAddress, editTextPortNumber;

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_settings, container, false);

        // assign text inputs
        editTextIPAddress = (EditText)layout.findViewById(R.id.editTextIPAddress);
        editTextPortNumber = (EditText)layout.findViewById(R.id.editTextPortNumber);

        // get the IP address and port number from the last time the user used the app,
        // put an empty string "" is this is the first time.
        editTextIPAddress.setText(SmartHomeActivity.sharedPreferences.getString(PREF_IP,""));
        editTextPortNumber.setText(SmartHomeActivity.sharedPreferences.getString(PREF_PORT,""));


        return layout;
    }

    public static EditText getEditTextIPAddress() {
        return editTextIPAddress;
    }

    public static EditText getEditTextPortNumber() {
        return editTextPortNumber;
    }
}
