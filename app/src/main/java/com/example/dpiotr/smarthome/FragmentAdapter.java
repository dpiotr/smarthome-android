package com.example.dpiotr.smarthome;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;


public class FragmentAdapter extends FragmentPagerAdapter {

    List<Fragment> listFragments;

    public FragmentAdapter(FragmentManager fm, List<android.support.v4.app.Fragment> listFragments) {
        super(fm);
        this.listFragments = listFragments;
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {
        return listFragments.get(position);
    }

    @Override
    public int getCount() {
        return listFragments.size();
    }

}
